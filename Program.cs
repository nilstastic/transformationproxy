using System.Collections.Concurrent;
using System.Text;
using System.Text.RegularExpressions;

var builder = WebApplication.CreateBuilder(args);
var config = builder.BindConfiguration<Configuration>("Proxy");
var app = builder.Build();

// file logging task
var logQueue = new ConcurrentQueue<string>();
Task.Run(async () => 
{
    while (true)
    {
        while (logQueue.TryDequeue(out var log))
        {
            File.AppendAllText(config.LogFilePath, log);
        }

        await Task.Delay(1000);
    }
});

// init route
app.Map("/", () => $"Let's go!");

// catch all route
app.Map("/{*all}", async (HttpContext context) => {

    var client = CreateClient();

    // match the path to a route and return the host
    var route = MatchRoute(context.Request.Path, config.Routes);

    // set the host as the destination
    client.BaseAddress = new Uri(route.DestinationHost);

    // add request headers to pass along to the server
    client.DefaultRequestHeaders.AddHeaders(context.Request.Headers);
    client.DefaultRequestHeaders.Remove("host");

    // get the incoming request body
    var requestBody = await context.Request.Body.ReadString();

    // create a new message, add content if available
    var request = CreateMessage(new HttpMethod(context.Request.Method), context.Request.Path, requestBody);

    // make a request to the matched route
    var response = await client.SendAsync(request);
    var responseText = await response.Content.ReadAsStringAsync();

    // transform the response
    responseText = TransformResponse(responseText, route.ResponseTransforms);

    // add response headers to pass back to the client
    context.Response.Headers.AddHeaders(response.Headers);

    // logging
    var logString = await GetLogString(request, response, requestBody, config.LogRequestBody, config.LogResponseBody);
    Console.WriteLine(logString);
    logQueue.Enqueue(logString);

    return responseText;
});

app.Run();

#region helpers
HttpClient CreateClient()
{
    return new HttpClient(new HttpClientHandler
    {
        ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true
    });
}

HttpRequestMessage CreateMessage(HttpMethod method, string path, string body)
{
    var request = new HttpRequestMessage(method, path);

    if (request.Method == HttpMethod.Post)
    {
        request.Content = new StringContent(body, Encoding.UTF8, "application/json");
    }

    return request;
}

Route MatchRoute(string path, List<Route> routes)
{
    foreach (var route in routes)
    {
        if (route.Enabled && Regex.IsMatch(path, route.Match))
        {
            return route;
        }
    }

    return null;
}

string TransformResponse(string responseString, IEnumerable<Transform> transforms)
{
    if (transforms == null)
    {
        return responseString;
    }
    
    foreach (var transform in transforms)
    {
        responseString = Regex.Replace(responseString, transform.Match, transform.Replacement);
    }

    return responseString;
}

async Task<string> GetLogString(HttpRequestMessage request, HttpResponseMessage response, string originalBody, bool logRequestBody, bool logResponseBody)
{
    var requestBody = logRequestBody ? originalBody.PrettyJson() : "";
    var responseBody = logResponseBody ? (await response.Content.ReadAsStringAsync()).PrettyJson() : "";

    return $"Time: {DateTime.Now}\nMethod: {request.Method}\nHost: {request.RequestUri.Host}\nUrl: {request.RequestUri.PathAndQuery}\nRequestBody: {requestBody}\nResponseBody: {responseBody}\n";
}
#endregion