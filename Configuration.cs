public class Configuration
{
    public string LogFilePath { get; set; } = default!;
    public bool LogRequestBody { get; set; }
    public bool LogResponseBody { get; set; }
    public List<Route> Routes { get; set; } = default!;
}

public class Route
{
    public bool Enabled { get; set; }
    public string Match { get; set; } = default!;
    public string DestinationHost { get; set; } = default!;
    public IEnumerable<Transform> ResponseTransforms { get; set; } = default!;
}

public class Transform
{
    public string Match { get; set; } = default!;
    public string Replacement { get; set; } = default!;
}