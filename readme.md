# TransformationProxy
A simple reverse proxy that allows for response transformations. The use case for this is to set this up between your backend and other backing api's and then either transform the response to mock new functionality or route to a mock server such as mockoon for further processing. Or both.

## Usage
Add a new route to the list of routes in the config.
```
{
    "Enabled": true,
    "Match": "/url/\\d+/",
    "DestinationHost": "http://localhost:3001/",
    "ResponseTransforms": [
        {
        "Match": "MatchThisRegex",
        "Replacement": "ReplaceWithThisValue"
        }
    ]
}
```

- `Match`: A regular expression that matches the route
- `DestinationHost`: The host to route the request to
- `ResponseTransforms`: Matches on the response string and replaces content in the response with a new value.