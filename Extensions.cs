using System.Net.Http.Headers;
using System.Text.Json;

public static class Extensions
{
    public static HttpHeaders AddHeaders(this HttpHeaders headers, IHeaderDictionary headersToCopy)
    {
        foreach (var header in headersToCopy)
        {
            headers.TryAddWithoutValidation(header.Key, header.Value.ToString());
        }

        return headers;
    }

    public static IHeaderDictionary AddHeaders(this IHeaderDictionary headers, HttpHeaders headersToCopy)
    {
        foreach (var header in headersToCopy)
        {
            headers.TryAdd(header.Key, header.Value.ToString());
        }

        return headers;
    }

    public static string PrettyJson(this string jsonString)
    {
        if (string.IsNullOrEmpty(jsonString))
        {
            return string.Empty;
        }

        try
        {
            using var jsonDocument = JsonDocument.Parse(jsonString);
            return JsonSerializer.Serialize(jsonDocument, new JsonSerializerOptions { WriteIndented = true });
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        return jsonString;
    }

    public static T BindConfiguration<T>(this WebApplicationBuilder builder, string section) where T : new()
    {
        var config = new T();
        builder.Configuration.GetSection(section).Bind(config);
        return config;
    }

    public static async Task<string> ReadString(this Stream stream)
    {
        using var streamReader = new StreamReader(stream);
        return await streamReader.ReadToEndAsync();
    }
}